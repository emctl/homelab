FROM linuxbrew/brew

# Install custom tools, runtime, etc.
RUN brew install graphviz python3 && \
  pip3 install diagrams
